# XRP Static Web Site

This is a static website with HTML, CSS and JavaScript files.  
It run just opening the index.html file with a browser.  
To create a XRP address the internet connection is not required.  
It allows to simply switch between the _TestNet_ and the _MainNet_ Ripple environments.
 
## Functionalities

- Create XRP address
- Verify XRP address
- Make a payment