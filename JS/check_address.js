let page = {
    page: this,

    init: () => {

        $("#TestNetButton").click( () => { 
            page.initializeApi(TEST_SERVER)                        
            $("#TestNetButton").addClass("btn-secondary"); $("#TestNetButton").removeClass("btn-outline-secondary")
            $("#MainNetButton").addClass("btn-outline-primary"); $("#MainNetButton").removeClass("btn-primary")  
            $("#customNetCheck").prop("checked", false)  
        })

        $("#MainNetButton").click( () => { 
            page.initializeApi(MAIN_SERVER)
            $("#MainNetButton").addClass("btn-primary"); $("#MainNetButton").removeClass("btn-outline-primary")       
            $("#TestNetButton").addClass("btn-outline-secondary"); $("#TestNetButton").removeClass("btn-secondary")
            $("#customNetCheck").prop("checked", false)
        })

        $("#customNetCheck").change((ev) =>    {        
            if (ev.target.checked) page.initializeApi($("#customNet").val())
            else $("#TestNetButton").click() // default
        })

        $("#submitButton").click( function() {   
            page.error()
            let address = $("#address").val().trim()  
            if (!address) page.error("The XRP Address must be specified") 
            else page.getAccountInfo(address)
        })

        if(sessionStorage.getItem("server") == MAIN_SERVER)
            $("#MainNetButton").click()
        else
            $("#TestNetButton").click() // default
    },

    initializeApi: (server) => {
        sessionStorage.setItem("server", server)
        console.log("server: " + server)
        api = new ripple.RippleAPI({server: server})
        api.on("error", (errorCode, errorMessage) => { page.error(`API error ${errorCode}. ${errorMessage}.`) })
    },

    error: (error) =>  {
        $("#error").text(error) 
        if (error) $("#error").show()
        else $("#error").hide()
    },

    getAccountInfo: (address) => {        
        $("#resultContainer").hide()
        //$("#resultContainer").text("fetching info for " + address)

        api.connect()
        .then(() => api.getAccountInfo(address))
        .then((info) => {
            page.showInfo(address, info)
            return api.disconnect()
        }).catch(page.error)
    },

    showInfo: (address, info) => {
        $("#resultContainer").show()

        $("#tdAddress").text(address)
        $("#tdXrp").text(info.xrpBalance)
        $("#tdSequence").text(info.sequence)
        $("#tdOwnerCount").text(info.ownerCount)
        $("#tdPreviousInitiatedTransactionID").text(info.previousInitiatedTransactionID)
        $("#tdPreviousAffectingTransactionID").text(info.previousAffectingTransactionID)
        $("#tdPreviousAffectingTransactionLedgerVersion").text(info.previousAffectingTransactionLedgerVersion)

        /*
        { sequence: 359,
  xrpBalance: '75.181663',
  ownerCount: 4,
  previousInitiatedTransactionID: 'E5C6DD25B2DCF534056D98A2EFE3B7CFAE4EBC624854DE3FA436F733A56D8BD9',
  previousAffectingTransactionID: 'E5C6DD25B2DCF534056D98A2EFE3B7CFAE4EBC624854DE3FA436F733A56D8BD9',
  previousAffectingTransactionLedgerVersion: 18489336 }
         */
    }
}

$("document").ready(function() {
    page.init()
})