const ALGORITHM_ECDSA = "ecdsa-secp256k1"
const ALGORITHM_ED25519= "ed25519"
// Optional. The digital signature algorithm to generate an address for. Can be ecdsa-secp256k1 (default) or ed25519.


let page = {
    page: this,

    init: () => {

        $("#TestNetButton").click( () => { 
            page.initializeApi(TEST_SERVER)
            $("#TestNetButton").addClass("btn-secondary"); $("#TestNetButton").removeClass("btn-outline-secondary")
            $("#MainNetButton").addClass("btn-outline-primary"); $("#MainNetButton").removeClass("btn-primary")    
        })

        $("#MainNetButton").click( () => { 
            page.initializeApi(MAIN_SERVER)
            $("#MainNetButton").addClass("btn-primary"); $("#MainNetButton").removeClass("btn-outline-primary")       
            $("#TestNetButton").addClass("btn-outline-secondary"); $("#TestNetButton").removeClass("btn-secondary")
        })

        $("#submitButton").click( function() {   
            page.error()
            let algorithm =  $("#alg_1").is(":checked") ? ALGORITHM_ECDSA : ALGORITHM_ED25519            
            page.createAddress(algorithm)
        })

        if(sessionStorage.getItem("server") == MAIN_SERVER)
            $("#MainNetButton").click()
        else
            $("#TestNetButton").click() // default
    },

    initializeApi: (server) => {
        sessionStorage.setItem("server", server)
        api = new ripple.RippleAPI({server: server})
        api.on("error", (errorCode, errorMessage) => { page.error(`API error ${errorCode}. ${errorMessage}.`) })
    },

    error: (error) =>  {
        $("#error").text(error) 
        if (error) $("#error").show()
        else $("#error").hide()
    },

    createAddress: (algorithm) => {
        $("#resultContainer").hide()
        try {
            let option = { algorithm: algorithm }
            let data = api.generateAddress(option)

            $("#resultContainer").show()
            $("#tdAlgorithm").text(algorithm)
            $("#tdAddress").text(data.address)
            $("#tdSecret").text(data.secret)
        }
        catch(error) {
            page.error(String(error))
        }
    }

}

$("document").ready(function() {
    page.init()
})