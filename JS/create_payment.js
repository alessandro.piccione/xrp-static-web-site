let page = {
    page: this,

// address format "^r[1-9A-HJ-NP-Za-km-z]{25,34}$")]

    init: () => {

        $("#TestNetButton").click( () => { 
            page.initializeApi(TEST_SERVER)
            $("#TestNetButton").addClass("btn-secondary"); $("#TestNetButton").removeClass("btn-outline-secondary")
            $("#MainNetButton").addClass("btn-outline-primary"); $("#MainNetButton").removeClass("btn-primary")    
        })

        $("#MainNetButton").click( () => { 
            page.initializeApi(MAIN_SERVER)
            $("#MainNetButton").addClass("btn-primary"); $("#MainNetButton").removeClass("btn-outline-primary")       
            $("#TestNetButton").addClass("btn-outline-secondary"); $("#TestNetButton").removeClass("btn-secondary")
        })

        $("#submitButton").click( function() {   
            page.error()
            try{
                page.createPayment()
            }
            catch(error) {page.error(String(error))}
        })

        if(sessionStorage.getItem("server") == MAIN_SERVER)
            $("#MainNetButton").click()
        else
            $("#TestNetButton").click() // default

        // dev setup
        $("#sourceAddress").val("rEqhz7vYYFmVfWJ7rPKd2EiDQDRAx4mvaP")
        $("#sourceSecret").text("ssMS15FUjt8AoWxf4N8e5GTkLdmwW")
        $("#destinationAddress").val("rsXVNtnAwoZtAxU6HSxzGZJDwipumoZFbZ")
    },

    initializeApi: (server) => {
        sessionStorage.setItem("server", server)
        console.log("server: " + server)
        api = new ripple.RippleAPI({server: server})
        api.on("error", (errorCode, errorMessage) => { page.error(`API error ${errorCode}. ${errorMessage}.`) })
    },

    error: (error) =>  {
        $("#error").text(error) 
        if (error) $("#error").show()
        else $("#error").hide()
    },

    createPayment: () => {
        $("#resultContainer").hide()       

        let _source = { address: $("#sourceAddress").val().trim(), secret: $("#sourceSecret").val().trim() }
        let _destination = { address: $("#destinationAddress").val().trim(), tag: $("#destinationTag").val().trim() }
        let amount = $("#amount").val().trim()

        if(!_source.address) throw Error("Source address must be specified")
        if(!_source.secret) throw Error("Source secret must be specified")
        if(!_destination.address) throw Error("Destination address must be specified")
        if(!amount) throw Error("Amount must be specified")

        if (!api.isValidAddress(_source.address)) throw Error("Source address is not valid")        
        if (!api.isValidSecret(_source.secret)) throw Error("Source secret is not valid")
        if (!api.isValidAddress(_destination.address)) throw Error("Destination address is not valid")

        let amount_drops = api.xrpToDrops(amount)    

        // https://xrpl.org/send-xrp.html
        
        $("#submitButton").attr("disabled", "disabled")

        page.showProgress("Connect")

        api.connect()
            .then(() => {

                page.showProgress("Prepare payment")

                let transactionData = {
                    "TransactionType": "Payment",
                    "Account": _source.address,
                    "Amount": amount_drops,
                    "Destination": _destination.address
                }

                if(_destination.tag != "") transactionData.DestinationTag = parseInt(_destination.tag) // MUST be an int

                let options = { 
                    // Expire the transaction if does not execute within 5 minutes
                    "maxLedgerVersionOffset": 75
                }

                api.prepareTransaction(transactionData, options)
                    .then((preparedTransaction) => {                     
// instructions  fee, sequence, maxLedgerverion, 
//txJSON
// console.log("Transaction expires after ledger:", maxLedgerVersion)   // from doc example, what deos it mean ?
                        page.showProgress("Sign payment")
                        //alert(preparedTransaction.txJSON)

                        const response = api.sign(preparedTransaction.txJSON, _source.secret)
                        const txID = response.id
                        const txBlob = response.signedTransaction
                        /* It's also a good idea to use the getLedgerVersion() method to take note of the latest validated ledger index 
                        before you submit. The earliest ledger version that your transaction could get into as a result of this submission 
                        is one higher than the latest validated ledger when you submit it. */
                        //page.showProgress("Get latest Ledger version")
                        api.getLedgerVersion().then((latestLedgerVersion) => {
                            page.showProgress("Submit signed transaction")

                            api.submit(txBlob).then((result) => {
                                api.disconnect()

                                page.showProgress(null)
                                $("#resultContainer").show()
                                $("#tdResult").addClass("text-success")
                                $("#tdResult").text(result.resultCode + " : " + result.resultMessage)

                                $("#tdFee").text(preparedTransaction.instructions.fee)
                                $("#tdSequence").text(preparedTransaction.instructions.sequence)
                                $("#tdTxID").text(txID)
                                
                                $("#submitButton").attr("disabled", null)

//### Wait validation and Check
//https://xrpl.org/send-xrp.html#interactive-sign

                            })
                        })
           
                    }).catch((error) => {
                        page.error("Failed to prepare payment. " + String(error))
                        $("#submitButton").attr("disabled", null)
                        return api.disconnect()
                    })

            }).catch((error) => {
                $("#submitButton").attr("disabled", null)    
                page.showProgress(null)
                page.error(error)
                api.disconnect()
            })
    },

    showProgress: (step) => {

        if(!step) return  $("#progressContainer").hide()

        $("#progressContainer").show()

        progress = "0%"
        switch(step) {
            case "Connect":
                progress = "25%"     
                break      
            case "Prepare payment": 
                progress = "50%"
                break
            case "Sign payment": 
                progress = "75%"
                break
            case "Submit signed transaction":
                progress = "95%"
                break
            default: alert("step unknown: " + step)                
        }
        $("#progressBar div").text(step)
        $("#progressBar div").css("width", progress)
    }

}

$("document").ready(function() {
    page.init()
})